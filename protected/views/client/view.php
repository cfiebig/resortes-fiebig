<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->CLIENT_RUT,
);

$this->menu=array(
	array('label'=>'Listar Clientes', 'url'=>array('index')),
	array('label'=>'Nuevo Cliente', 'url'=>array('create')),
	array('label'=>'Actualizar Cliente', 'url'=>array('update', 'id'=>$model->CLIENT_RUT)),
	array('label'=>'Eliminar Cliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->CLIENT_RUT),'confirm'=>'Esta seguro de eliminar el siguiente cliente?')),
	array('label'=>'Administar Clientes', 'url'=>array('admin')),
);
?>

<h1>Visualizar Cliente #<?php echo $model->CLIENT_RUT; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'CLIENT_RUT',
		'CLIENT_NAME',
		'CLIENT_EMAIL',
		'CLIENT_CELL_PHONE',
		'CLIENT_PHONE',
		'CLIENT_FAX',
	),
)); ?>
