<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_RUT'); ?>
		<?php echo $form->textField($model,'CLIENT_RUT',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_NAME'); ?>
		<?php echo $form->textField($model,'CLIENT_NAME',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_EMAIL'); ?>
		<?php echo $form->textField($model,'CLIENT_EMAIL',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_CELL_PHONE'); ?>
		<?php echo $form->textField($model,'CLIENT_CELL_PHONE',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_PHONE'); ?>
		<?php echo $form->textField($model,'CLIENT_PHONE',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CLIENT_FAX'); ?>
		<?php echo $form->textField($model,'CLIENT_FAX',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->