<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->CLIENT_RUT=>array('view','id'=>$model->CLIENT_RUT),
	'Actualizacion',
);

$this->menu=array(
	array('label'=>'Listar Clientes', 'url'=>array('index')),
	array('label'=>'Nuevo Cliente', 'url'=>array('create')),
	array('label'=>'Visualizar Cliente', 'url'=>array('view', 'id'=>$model->CLIENT_RUT)),
	array('label'=>'Administrar Clientes', 'url'=>array('admin')),
);
?>

<h1>Actualizar Cliente <?php echo $model->CLIENT_RUT; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>