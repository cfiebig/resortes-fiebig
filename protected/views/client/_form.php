<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_RUT'); ?>
		<?php echo $form->textField($model,'CLIENT_RUT',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'CLIENT_RUT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_NAME'); ?>
		<?php echo $form->textField($model,'CLIENT_NAME',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'CLIENT_NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_EMAIL'); ?>
		<?php echo $form->textField($model,'CLIENT_EMAIL',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'CLIENT_EMAIL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_CELL_PHONE'); ?>
		<?php echo $form->textField($model,'CLIENT_CELL_PHONE',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'CLIENT_CELL_PHONE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_PHONE'); ?>
		<?php echo $form->textField($model,'CLIENT_PHONE',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'CLIENT_PHONE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CLIENT_FAX'); ?>
		<?php echo $form->textField($model,'CLIENT_FAX',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'CLIENT_FAX'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->