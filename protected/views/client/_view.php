<?php
/* @var $this ClientController */
/* @var $data Client */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_RUT')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CLIENT_RUT), array('view', 'id'=>$data->CLIENT_RUT)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->CLIENT_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_EMAIL')); ?>:</b>
	<?php echo CHtml::encode($data->CLIENT_EMAIL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_CELL_PHONE')); ?>:</b>
	<?php echo CHtml::encode($data->CLIENT_CELL_PHONE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_PHONE')); ?>:</b>
	<?php echo CHtml::encode($data->CLIENT_PHONE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CLIENT_FAX')); ?>:</b>
	<?php echo CHtml::encode($data->CLIENT_FAX); ?>
	<br />


</div>