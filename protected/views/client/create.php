<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Creacion',
);

$this->menu=array(
	array('label'=>'Listar Clientes', 'url'=>array('index')),
	array('label'=>'Administrar Clientes', 'url'=>array('admin')),
);
?>

<h1>Nuevo Cliente</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>