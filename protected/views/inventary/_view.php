<?php
/* @var $this InventaryController */
/* @var $data Inventary */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_CODE')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ITEM_CODE), array('view', 'id'=>$data->ITEM_CODE)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->ITEM_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_TYPE')); ?>:</b>
	<?php echo CHtml::encode($data->ITEM_TYPE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_MEASURE_SYSTEM')); ?>:</b>
	<?php echo CHtml::encode($data->ITEM_MEASURE_SYSTEM); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_MINIMUN_STOCK')); ?>:</b>
	<?php echo CHtml::encode($data->ITEM_MINIMUN_STOCK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ITEM_STOCK')); ?>:</b>
	<?php echo CHtml::encode($data->ITEM_STOCK); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_DET_UNIT_PRICE')); ?>:</b>
	<?php 
	/*
		foreach($data->purchaseDetail as $pd)
		{
			//echo $pd->PURCHASE_DET_UNIT_PRICE; 
		}
		*/
	?>
	<br />


</div>