<?php
/* @var $this InventaryController */
/* @var $model Inventary */

$this->breadcrumbs=array(
	'Inventaries'=>array('index'),
	$model->ITEM_CODE=>array('view','id'=>$model->ITEM_CODE),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Inventario', 'url'=>array('index')),
	array('label'=>'Crear Inventario', 'url'=>array('create')),
	array('label'=>'Ver Inventario', 'url'=>array('view', 'id'=>$model->ITEM_CODE)),
	array('label'=>'Editar Inventario', 'url'=>array('admin')),
);
?>

<h1>Update Inventary <?php echo $model->ITEM_CODE; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>