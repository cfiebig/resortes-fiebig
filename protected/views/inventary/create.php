<?php
/* @var $this InventaryController */
/* @var $model Inventary */

$this->breadcrumbs=array(
	'Inventaries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Inventario', 'url'=>array('index')),
	array('label'=>'Editar Inventario', 'url'=>array('admin')),
);
?>

<h1>Create Inventary</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>