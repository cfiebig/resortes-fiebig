<?php
/* @var $this InventaryController */
/* @var $model Inventary */

$this->breadcrumbs=array(
	'Inventaries'=>array('index'),
	$model->ITEM_CODE,
);

$this->menu=array(
	array('label'=>'Listar Inventario', 'url'=>array('index')),
	array('label'=>'Crear Inventario', 'url'=>array('create')),
	array('label'=>'Actualizar Inventario', 'url'=>array('update', 'id'=>$model->ITEM_CODE)),
	array('label'=>'Eliminar Inventario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ITEM_CODE),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Editar Inventario', 'url'=>array('admin')),
);
?>

<h1>View Inventary #<?php echo $model->ITEM_CODE; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ITEM_CODE',
		'ITEM_NAME',
		'ITEM_TYPE',
		'ITEM_MEASURE_SYSTEM',
		'ITEM_MINIMUN_STOCK',
		'ITEM_STOCK',
	),
)); ?>
