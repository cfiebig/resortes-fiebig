<?php
/* @var $this InventaryController */
/* @var $model Inventary */

$this->breadcrumbs=array(
	'Inventaries'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Listar Inventario', 'url'=>array('index')),
	array('label'=>'Editar Inventario', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventary-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Inventaries</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inventary-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ITEM_CODE',
		'ITEM_NAME',
		'ITEM_TYPE',
		'ITEM_MEASURE_SYSTEM',
		'ITEM_MINIMUN_STOCK',
		'ITEM_STOCK',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
