<?php
/* @var $this InventaryController */
/* @var $model Inventary */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ITEM_CODE'); ?>
		<?php echo $form->textField($model,'ITEM_CODE',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ITEM_NAME'); ?>
		<?php echo $form->textField($model,'ITEM_NAME',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ITEM_TYPE'); ?>
		<?php echo $form->textField($model,'ITEM_TYPE',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ITEM_MEASURE_SYSTEM'); ?>
		<?php echo $form->textField($model,'ITEM_MEASURE_SYSTEM',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ITEM_MINIMUN_STOCK'); ?>
		<?php echo $form->textField($model,'ITEM_MINIMUN_STOCK'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ITEM_STOCK'); ?>
		<?php echo $form->textField($model,'ITEM_STOCK'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->