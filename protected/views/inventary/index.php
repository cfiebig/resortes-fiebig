<?php
/* @var $this InventaryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inventaries',
);

$this->menu=array(
	array('label'=>'Listar Inventario', 'url'=>array('create')),
	array('label'=>'Editar Inventario', 'url'=>array('admin')),
);
?>

<h1>Inventaries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
