<?php
/* @var $this InventaryController */
/* @var $model Inventary */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventary-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_CODE'); ?>
		<?php echo $form->textField($model,'ITEM_CODE',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'ITEM_CODE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_NAME'); ?>
		<?php echo $form->textField($model,'ITEM_NAME',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'ITEM_NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_TYPE'); ?>
		<?php echo $form->textField($model,'ITEM_TYPE',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'ITEM_TYPE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_MEASURE_SYSTEM'); ?>
		<?php echo $form->textField($model,'ITEM_MEASURE_SYSTEM',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ITEM_MEASURE_SYSTEM'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_MINIMUN_STOCK'); ?>
		<?php echo $form->textField($model,'ITEM_MINIMUN_STOCK'); ?>
		<?php echo $form->error($model,'ITEM_MINIMUN_STOCK'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ITEM_STOCK'); ?>
		<?php echo $form->textField($model,'ITEM_STOCK'); ?>
		<?php echo $form->error($model,'ITEM_STOCK'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->