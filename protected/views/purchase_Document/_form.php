<?php
/* @var $this Purchase_DocumentController */
/* @var $model Purchase_Document */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchase--document-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_RUT'); ?>
		<?php echo $form->textField($model,'PROVIDER_RUT',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'PROVIDER_RUT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PURCHASE_TYPE_PAYMENT'); ?>
		<?php echo $form->textField($model,'PURCHASE_TYPE_PAYMENT',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'PURCHASE_TYPE_PAYMENT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PURCHASE_TOTAL'); ?>
		<?php echo $form->textField($model,'PURCHASE_TOTAL'); ?>
		<?php echo $form->error($model,'PURCHASE_TOTAL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PURCHASE_STATUS'); ?>
		<?php echo $form->textField($model,'PURCHASE_STATUS',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'PURCHASE_STATUS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PURCHASE_DATE'); ?>
		<?php echo $form->textField($model,'PURCHASE_DATE'); ?>
		<?php echo $form->error($model,'PURCHASE_DATE'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->