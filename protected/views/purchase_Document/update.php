<?php
/* @var $this Purchase_DocumentController */
/* @var $model Purchase_Document */

$this->breadcrumbs=array(
	'Purchase  Documents'=>array('index'),
	$model->PURCHASE_ID=>array('view','id'=>$model->PURCHASE_ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Purchase_Document', 'url'=>array('index')),
	array('label'=>'Create Purchase_Document', 'url'=>array('create')),
	array('label'=>'View Purchase_Document', 'url'=>array('view', 'id'=>$model->PURCHASE_ID)),
	array('label'=>'Manage Purchase_Document', 'url'=>array('admin')),
);
?>

<h1>Update Purchase_Document <?php echo $model->PURCHASE_ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>