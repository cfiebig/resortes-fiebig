<?php
/* @var $this Purchase_DocumentController */
/* @var $model Purchase_Document */

$this->breadcrumbs=array(
	'Purchase  Documents'=>array('index'),
	$model->PURCHASE_ID,
);

$this->menu=array(
	array('label'=>'List Purchase_Document', 'url'=>array('index')),
	array('label'=>'Create Purchase_Document', 'url'=>array('create')),
	array('label'=>'Update Purchase_Document', 'url'=>array('update', 'id'=>$model->PURCHASE_ID)),
	array('label'=>'Delete Purchase_Document', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->PURCHASE_ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Purchase_Document', 'url'=>array('admin')),
);
?>

<h1>View Purchase_Document #<?php echo $model->PURCHASE_ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'PURCHASE_ID',
		'PROVIDER_RUT',
		'PURCHASE_TYPE_PAYMENT',
		'PURCHASE_TOTAL',
		'PURCHASE_STATUS',
		'PURCHASE_DATE',
	),
)); ?>
