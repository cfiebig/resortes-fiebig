<?php
/* @var $this Purchase_DocumentController */
/* @var $data Purchase_Document */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->PURCHASE_ID), array('view', 'id'=>$data->PURCHASE_ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_RUT')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_RUT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_TYPE_PAYMENT')); ?>:</b>
	<?php echo CHtml::encode($data->PURCHASE_TYPE_PAYMENT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_TOTAL')); ?>:</b>
	<?php echo CHtml::encode($data->PURCHASE_TOTAL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_STATUS')); ?>:</b>
	<?php echo CHtml::encode($data->PURCHASE_STATUS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PURCHASE_DATE')); ?>:</b>
	<?php echo CHtml::encode($data->PURCHASE_DATE); ?>
	<br />


</div>