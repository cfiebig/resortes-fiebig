<?php
/* @var $this Purchase_DocumentController */
/* @var $model Purchase_Document */

$this->breadcrumbs=array(
	'Purchase  Documents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Purchase_Document', 'url'=>array('index')),
	array('label'=>'Manage Purchase_Document', 'url'=>array('admin')),
);
?>

<h1>Create Purchase_Document</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>