<?php
/* @var $this Purchase_DocumentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Purchase  Documents',
);

$this->menu=array(
	array('label'=>'Create Purchase_Document', 'url'=>array('create')),
	array('label'=>'Manage Purchase_Document', 'url'=>array('admin')),
);
?>

<h1>Purchase  Documents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
