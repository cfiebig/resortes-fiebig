<?php
/* @var $this Purchase_DocumentController */
/* @var $model Purchase_Document */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'PURCHASE_ID'); ?>
		<?php echo $form->textField($model,'PURCHASE_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_RUT'); ?>
		<?php echo $form->textField($model,'PROVIDER_RUT',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PURCHASE_TYPE_PAYMENT'); ?>
		<?php echo $form->textField($model,'PURCHASE_TYPE_PAYMENT',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PURCHASE_TOTAL'); ?>
		<?php echo $form->textField($model,'PURCHASE_TOTAL'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PURCHASE_STATUS'); ?>
		<?php echo $form->textField($model,'PURCHASE_STATUS',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PURCHASE_DATE'); ?>
		<?php echo $form->textField($model,'PURCHASE_DATE'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->