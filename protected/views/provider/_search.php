<?php
/* @var $this ProviderController */
/* @var $model provider */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_RUT'); ?>
		<?php echo $form->textField($model,'PROVIDER_RUT',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_NAME'); ?>
		<?php echo $form->textField($model,'PROVIDER_NAME',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_EMAIL'); ?>
		<?php echo $form->textField($model,'PROVIDER_EMAIL',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_CELL_PHONE'); ?>
		<?php echo $form->textField($model,'PROVIDER_CELL_PHONE',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_PHONE'); ?>
		<?php echo $form->textField($model,'PROVIDER_PHONE',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PROVIDER_FAX'); ?>
		<?php echo $form->textField($model,'PROVIDER_FAX',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->