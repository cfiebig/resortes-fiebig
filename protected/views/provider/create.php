<?php
/* @var $this ProviderController */
/* @var $model provider */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	'Creacion',
);

$this->menu=array(
	array('label'=>'Listar Proveedores', 'url'=>array('index')),
	array('label'=>'Administrar Proveedores', 'url'=>array('admin')),
);
?>

<h1>Nuevo Proveedor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>