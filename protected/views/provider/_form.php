<?php
/* @var $this ProviderController */
/* @var $model provider */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'provider-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_RUT'); ?>
		<?php echo $form->textField($model,'PROVIDER_RUT',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'PROVIDER_RUT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_NAME'); ?>
		<?php echo $form->textField($model,'PROVIDER_NAME',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'PROVIDER_NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_EMAIL'); ?>
		<?php echo $form->textField($model,'PROVIDER_EMAIL',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'PROVIDER_EMAIL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_CELL_PHONE'); ?>
		<?php echo $form->textField($model,'PROVIDER_CELL_PHONE',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'PROVIDER_CELL_PHONE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_PHONE'); ?>
		<?php echo $form->textField($model,'PROVIDER_PHONE',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'PROVIDER_PHONE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PROVIDER_FAX'); ?>
		<?php echo $form->textField($model,'PROVIDER_FAX',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'PROVIDER_FAX'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->