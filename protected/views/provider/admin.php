<?php
/* @var $this ProviderController */
/* @var $model provider */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	'Administracion',
);

$this->menu=array(
	array('label'=>'Listar Proveedores', 'url'=>array('index')),
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('provider-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administracion de Proveedores</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'provider-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'PROVIDER_RUT',
		'PROVIDER_NAME',
		'PROVIDER_EMAIL',
		'PROVIDER_CELL_PHONE',
		'PROVIDER_PHONE',
		'PROVIDER_FAX',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
