<?php
/* @var $this ProviderController */
/* @var $model provider */

$this->breadcrumbs=array(
	'Proveedores'=>array('history'),
	'Historial',
);

$this->menu=array(
	array('label'=>'Listar Proveedores', 'url'=>array('index')),
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
	array('label'=>'Visualizar Proveedor', 'url'=>array('view', 'id'=>$model->PROVIDER_RUT)),
	array('label'=>'Administrar Proveedores', 'url'=>array('admin')),
);

?>

<h1>Historial Proveedor del Proveedor </h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>PROVIDER_RUT,
	'attributes'=>array(
		'PROVIDER_RUT',
		'PROVIDER_NAME',
		'PROVIDER_EMAIL',
		'PROVIDER_CELL_PHONE',
		'PROVIDER_PHONE',
		'PROVIDER_FAX',
	),
));
echo $model; ?>
