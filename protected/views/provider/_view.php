<?php
/* @var $this ProviderController */
/* @var $data provider */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_RUT')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->PROVIDER_RUT), array('view', 'id'=>$data->PROVIDER_RUT)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_EMAIL')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_EMAIL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_CELL_PHONE')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_CELL_PHONE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_PHONE')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_PHONE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PROVIDER_FAX')); ?>:</b>
	<?php echo CHtml::encode($data->PROVIDER_FAX); ?>
	<br />


</div>