<?php
/* @var $this ProviderController */
/* @var $model provider */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->PROVIDER_RUT=>array('view','id'=>$model->PROVIDER_RUT),
	'Actualizacion',
);

$this->menu=array(
	array('label'=>'Listar Proveedores', 'url'=>array('index')),
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
	array('label'=>'Visualizar Proveedor', 'url'=>array('view', 'id'=>$model->PROVIDER_RUT)),
	array('label'=>'Administrar Proveedores', 'url'=>array('admin')),
);
?>

<h1>Actualizar Proveedor<?php echo $model->PROVIDER_RUT; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>