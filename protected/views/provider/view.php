<?php
/* @var $this ProviderController */
/* @var $model provider */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->PROVIDER_RUT,
);

$this->menu=array(
	array('label'=>'Listar Proveedores', 'url'=>array('index')),
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
	array('label'=>'Actualizar Proveedor', 'url'=>array('update', 'id'=>$model->PROVIDER_RUT)),
	array('label'=>'Eliminar Proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->PROVIDER_RUT),'confirm'=>'Esta seguro de eliminar el siguiente proveedor?')),
	array('label'=>'Administrar Proveedores', 'url'=>array('admin')),
	array('label'=>'Historial de Proveedor', 'url'=>array('history')),
);
?>

<h1>Visualizar Proveedor #<?php echo $model->PROVIDER_RUT; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'PROVIDER_RUT',
		'PROVIDER_NAME',
		'PROVIDER_EMAIL',
		'PROVIDER_CELL_PHONE',
		'PROVIDER_PHONE',
		'PROVIDER_FAX',
	),
)); ?>
