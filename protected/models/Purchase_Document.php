<?php

/**
 * This is the model class for table "PURCHASE_DOCUMENT".
 *
 * The followings are the available columns in table 'PURCHASE_DOCUMENT':
 * @property integer $PURCHASE_ID
 * @property string $PROVIDER_RUT
 * @property string $PURCHASE_TYPE_PAYMENT
 * @property integer $PURCHASE_TOTAL
 * @property string $PURCHASE_STATUS
 * @property string $PURCHASE_DATE
 */
class Purchase_Document extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Purchase_Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PURCHASE_DOCUMENT';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PROVIDER_RUT, PURCHASE_TYPE_PAYMENT, PURCHASE_TOTAL, PURCHASE_STATUS, PURCHASE_DATE', 'required'),
			array('PURCHASE_TOTAL', 'numerical', 'integerOnly'=>true),
			array('PROVIDER_RUT', 'length', 'max'=>20),
			array('PURCHASE_TYPE_PAYMENT', 'length', 'max'=>64),
			array('PURCHASE_STATUS', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('PURCHASE_ID, PROVIDER_RUT, PURCHASE_TYPE_PAYMENT, PURCHASE_TOTAL, PURCHASE_STATUS, PURCHASE_DATE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'provider'=>array(self::BELONG_TO,'PROVIDER','PROVIDER_RUT'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PURCHASE_ID' => 'Purchase',
			'PROVIDER_RUT' => 'Provider Rut',
			'PURCHASE_TYPE_PAYMENT' => 'Purchase Type Payment',
			'PURCHASE_TOTAL' => 'Purchase Total',
			'PURCHASE_STATUS' => 'Purchase Status',
			'PURCHASE_DATE' => 'Purchase Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('PURCHASE_ID',$this->PURCHASE_ID);
		$criteria->compare('PROVIDER_RUT',$this->PROVIDER_RUT,true);
		$criteria->compare('PURCHASE_TYPE_PAYMENT',$this->PURCHASE_TYPE_PAYMENT,true);
		$criteria->compare('PURCHASE_TOTAL',$this->PURCHASE_TOTAL);
		$criteria->compare('PURCHASE_STATUS',$this->PURCHASE_STATUS,true);
		$criteria->compare('PURCHASE_DATE',$this->PURCHASE_DATE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}