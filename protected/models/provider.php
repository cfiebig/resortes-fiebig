<?php

/**
 * This is the model class for table "PROVIDER".
 *
 * The followings are the available columns in table 'PROVIDER':
 * @property string $PROVIDER_RUT
 * @property string $PROVIDER_NAME
 * @property string $PROVIDER_EMAIL
 * @property string $PROVIDER_CELL_PHONE
 * @property string $PROVIDER_PHONE
 * @property string $PROVIDER_FAX
 */
class provider extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return provider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PROVIDER';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PROVIDER_RUT, PROVIDER_NAME', 'required'),
			array('PROVIDER_RUT', 'length', 'max'=>20),
			array('PROVIDER_NAME, PROVIDER_EMAIL', 'length', 'max'=>100),
			array('PROVIDER_CELL_PHONE, PROVIDER_PHONE, PROVIDER_FAX', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('PROVIDER_RUT, PROVIDER_NAME, PROVIDER_EMAIL, PROVIDER_CELL_PHONE, PROVIDER_PHONE, PROVIDER_FAX', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'history'=>array(self::HAS_MANY,'PURCHASE_DOCUMENT','PURCHASE_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PROVIDER_RUT' => 'RUTa',
			'PROVIDER_NAME' => 'Nombre',
			'PROVIDER_EMAIL' => 'Email',
			'PROVIDER_CELL_PHONE' => 'Telefono Celular',
			'PROVIDER_PHONE' => 'Telefono Fijo',
			'PROVIDER_FAX' => 'Telefono Fax',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('PROVIDER_RUT',$this->PROVIDER_RUT,true);
		$criteria->compare('PROVIDER_NAME',$this->PROVIDER_NAME,true);
		$criteria->compare('PROVIDER_EMAIL',$this->PROVIDER_EMAIL,true);
		$criteria->compare('PROVIDER_CELL_PHONE',$this->PROVIDER_CELL_PHONE,true);
		$criteria->compare('PROVIDER_PHONE',$this->PROVIDER_PHONE,true);
		$criteria->compare('PROVIDER_FAX',$this->PROVIDER_FAX,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}