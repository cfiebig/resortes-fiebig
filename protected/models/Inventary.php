<?php

/**
 * This is the model class for table "INVENTARY".
 *
 * The followings are the available columns in table 'INVENTARY':
 * @property string $ITEM_CODE
 * @property string $ITEM_NAME
 * @property string $ITEM_TYPE
 * @property string $ITEM_MEASURE_SYSTEM
 * @property integer $ITEM_MINIMUN_STOCK
 * @property integer $ITEM_STOCK
 */
class Inventary extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Inventary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'INVENTARY';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ITEM_CODE, ITEM_NAME, ITEM_TYPE, ITEM_MEASURE_SYSTEM, ITEM_MINIMUN_STOCK, ITEM_STOCK', 'required'),
			array('ITEM_MINIMUN_STOCK, ITEM_STOCK', 'numerical', 'integerOnly'=>true),
			array('ITEM_CODE', 'length', 'max'=>15),
			array('ITEM_NAME, ITEM_TYPE', 'length', 'max'=>64),
			array('ITEM_MEASURE_SYSTEM', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ITEM_CODE, ITEM_NAME, ITEM_TYPE, ITEM_MEASURE_SYSTEM, ITEM_MINIMUN_STOCK, ITEM_STOCK', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'purchaseDetails' => array(self::MANY_MANY, 'PURCHASE_DETAIL', 'INVENTARY_PURCHASE_DETAIL(ITEM_CODE,PURCHASE_DET_ID)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ITEM_CODE' => 'Codigo Item',
			'ITEM_NAME' => 'Nombre Item',
			'ITEM_TYPE' => 'Tipo Item',
			'ITEM_MEASURE_SYSTEM' => 'Sistema de Medicion',
			'ITEM_MINIMUN_STOCK' => 'Stock Minimo',
			'ITEM_STOCK' => 'Stock Actual',
			'PURCHASE_DET_UNIT_PRICE' => 'Precio Unitario ultima compra',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ITEM_CODE',$this->ITEM_CODE,true);
		$criteria->compare('ITEM_NAME',$this->ITEM_NAME,true);
		$criteria->compare('ITEM_TYPE',$this->ITEM_TYPE,true);
		$criteria->compare('ITEM_MEASURE_SYSTEM',$this->ITEM_MEASURE_SYSTEM,true);
		$criteria->compare('ITEM_MINIMUN_STOCK',$this->ITEM_MINIMUN_STOCK);
		$criteria->compare('ITEM_STOCK',$this->ITEM_STOCK);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}