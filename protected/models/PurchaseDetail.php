<?php

/**
 * This is the model class for table "PURCHASE_DETAIL".
 *
 * The followings are the available columns in table 'PURCHASE_DETAIL':
 * @property integer $PURCHASE_DET_ID
 * @property integer $PURCHASE_ID
 * @property double $PURCHASE_DET_QTY
 * @property integer $PURCHASE_DET_UNIT_PRICE
 * @property double $PURCHASE_DET_TOTAL
 */
class PurchaseDetail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PurchaseDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PURCHASE_DETAIL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PURCHASE_ID, PURCHASE_DET_QTY, PURCHASE_DET_UNIT_PRICE, PURCHASE_DET_TOTAL', 'required'),
			array('PURCHASE_ID, PURCHASE_DET_UNIT_PRICE', 'numerical', 'integerOnly'=>true),
			array('PURCHASE_DET_QTY, PURCHASE_DET_TOTAL', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('PURCHASE_DET_ID, PURCHASE_ID, PURCHASE_DET_QTY, PURCHASE_DET_UNIT_PRICE, PURCHASE_DET_TOTAL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inventaries' => array(self::MANY_MANY, 'INVENTARY', 'INVENTARY_PURCHASE_DETAIL(PURCHASE_DET_ID,ITEM_CODE)'),
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PURCHASE_DET_ID' => 'Purchase Det',
			'PURCHASE_ID' => 'Purchase',
			'PURCHASE_DET_QTY' => 'Purchase Det Qty',
			'PURCHASE_DET_UNIT_PRICE' => 'Precio Unitario ultima compra',
			'PURCHASE_DET_TOTAL' => 'Purchase Det Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('PURCHASE_DET_ID',$this->PURCHASE_DET_ID);
		$criteria->compare('PURCHASE_ID',$this->PURCHASE_ID);
		$criteria->compare('PURCHASE_DET_QTY',$this->PURCHASE_DET_QTY);
		$criteria->compare('PURCHASE_DET_UNIT_PRICE',$this->PURCHASE_DET_UNIT_PRICE);
		$criteria->compare('PURCHASE_DET_TOTAL',$this->PURCHASE_DET_TOTAL);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}