<?php

/**
 * This is the model class for table "CLIENT".
 *
 * The followings are the available columns in table 'CLIENT':
 * @property string $CLIENT_RUT
 * @property string $CLIENT_NAME
 * @property string $CLIENT_EMAIL
 * @property string $CLIENT_CELL_PHONE
 * @property string $CLIENT_PHONE
 * @property string $CLIENT_FAX
 */
class Client extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CLIENT';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CLIENT_RUT, CLIENT_NAME', 'required'),
			array('CLIENT_RUT', 'length', 'max'=>20),
			array('CLIENT_NAME, CLIENT_EMAIL', 'length', 'max'=>100),
			array('CLIENT_CELL_PHONE, CLIENT_PHONE, CLIENT_FAX', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('CLIENT_RUT, CLIENT_NAME, CLIENT_EMAIL, CLIENT_CELL_PHONE, CLIENT_PHONE, CLIENT_FAX', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CLIENT_RUT' => 'RUT',
			'CLIENT_NAME' => 'Nombre',
			'CLIENT_EMAIL' => 'Email',
			'CLIENT_CELL_PHONE' => 'Telefono Celular',
			'CLIENT_PHONE' => 'Telefono Fijo',
			'CLIENT_FAX' => 'Telefono Fax',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CLIENT_RUT',$this->CLIENT_RUT,true);
		$criteria->compare('CLIENT_NAME',$this->CLIENT_NAME,true);
		$criteria->compare('CLIENT_EMAIL',$this->CLIENT_EMAIL,true);
		$criteria->compare('CLIENT_CELL_PHONE',$this->CLIENT_CELL_PHONE,true);
		$criteria->compare('CLIENT_PHONE',$this->CLIENT_PHONE,true);
		$criteria->compare('CLIENT_FAX',$this->CLIENT_FAX,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}